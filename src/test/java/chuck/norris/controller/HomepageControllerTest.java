package chuck.norris.controller;

import chuck.norris.domain.RemoteJoke;
import chuck.norris.services.ChuckNorrisJokeService;
import com.google.common.collect.ImmutableSet;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.servlet.ModelAndView;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class HomepageControllerTest {
    private static final long JOKE_ID = 1L;
    private static final String THE_JOKE = "joke";
    private static final String CATEGORY = "category";
    private static final RemoteJoke JOKE = new RemoteJoke(JOKE_ID, THE_JOKE, ImmutableSet.of(CATEGORY));

    @Mock
    private ChuckNorrisJokeService chuckNorrisJokeService;

    @InjectMocks
    private HomepageController objectUnderTest;

    @Before
    public void initMock() {
        when(chuckNorrisJokeService.getJokeFromRemote()).thenReturn(JOKE);
    }

    @Test
    public void requestJokeFromRemoteServiceAndForwardToHomeTemplate() {
        final ModelAndView modelAndView = objectUnderTest.homepage();
        assertThat(modelAndView.getViewName(), is("home"));
        verify(chuckNorrisJokeService).getJokeFromRemote();
    }
}