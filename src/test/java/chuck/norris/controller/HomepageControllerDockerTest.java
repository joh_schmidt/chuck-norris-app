package chuck.norris.controller;

import chuck.norris.DockerTest;
import com.jayway.restassured.RestAssured;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.test.context.ActiveProfiles;

import static chuck.norris.services.IcndbMock.MOCK_JOKE;
import static com.jayway.restassured.RestAssured.get;
import static org.hamcrest.Matchers.containsString;

@Category(DockerTest.class)
@ActiveProfiles("integrationTest")
public class HomepageControllerDockerTest {
    @Before
    public void setup() {
        RestAssured.baseURI = System.getProperty("app.url", "http://localhost:8080");
    }

    @Test
    public void gettingHomepage() throws Exception {
        get("/").then().assertThat().statusCode(200);
        get("/").then().assertThat().content(containsString(MOCK_JOKE));
    }
}