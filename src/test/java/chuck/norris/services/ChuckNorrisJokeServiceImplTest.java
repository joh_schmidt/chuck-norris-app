package chuck.norris.services;

import chuck.norris.domain.IcndbResponse;
import chuck.norris.domain.RemoteJoke;
import chuck.norris.persistence.Joke;
import chuck.norris.persistence.JokeRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class ChuckNorrisJokeServiceImplTest {
    @Mock
    private RestTemplate restTemplate;

    @Mock
    private JokeRepository jokeRepository;

    @InjectMocks
    private ChuckNorrisJokeServiceImpl objectUnderTest;

    public static ResponseEntity<IcndbResponse> buildResponse(final RemoteJoke remoteJoke) {
        final IcndbResponse icndbResponse = new IcndbResponse();
        ReflectionTestUtils.setField(icndbResponse, "type", "success");
        ReflectionTestUtils.setField(icndbResponse, "value", remoteJoke);

        return ResponseEntity.ok(icndbResponse);
    }

    @Test
    public void getJokeFromRemote() {
        final RemoteJoke expectedJoke = new RemoteJoke(1L, "The Joke.", Collections.singleton("nerdy"));

        when(restTemplate.getForEntity(anyString(), eq(IcndbResponse.class))).thenReturn(buildResponse(expectedJoke));

        final RemoteJoke jokeFromRemote = objectUnderTest.getJokeFromRemote();

        assertThat(jokeFromRemote, is(expectedJoke));
        verify(jokeRepository).save(any(Joke.class));
    }
}