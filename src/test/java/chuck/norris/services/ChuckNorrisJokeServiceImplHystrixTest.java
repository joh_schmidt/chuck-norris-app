package chuck.norris.services;

import chuck.norris.domain.IcndbResponse;
import chuck.norris.domain.RemoteJoke;
import chuck.norris.persistence.Joke;
import chuck.norris.persistence.JokeRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.JpaBaseConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

import static chuck.norris.services.ChuckNorrisJokeServiceImplTest.buildResponse;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration
public class ChuckNorrisJokeServiceImplHystrixTest {
    private static final String DB_JOKE = "db joke";
    private static final String REMOTE_JOKE = "remote joke";

    @Autowired
    private ChuckNorrisJokeService chuckNorrisJokeService;

    @Test
    public void fallbackIfRemoteCallFails() {
        assertThat(chuckNorrisJokeService.getJokeFromRemote().getJoke(), is(DB_JOKE));
        assertThat(chuckNorrisJokeService.getJokeFromRemote().getJoke(), is(REMOTE_JOKE));
    }

    @Configuration
    @EnableAutoConfiguration(exclude = {JpaBaseConfiguration.class, DataSourceAutoConfiguration.class, HibernateJpaAutoConfiguration.class})
    @EnableCircuitBreaker
    @ComponentScan(basePackageClasses = ChuckNorrisJokeService.class)
    public static class SpringConfig {
        @Bean
        @Primary
        public RestTemplate restTemplate() {
            final RemoteJoke remoteJoke = new RemoteJoke(1L, REMOTE_JOKE, Collections.singleton("nerdy"));

            final RestTemplate restTemplate = mock(RestTemplate.class);

            when(restTemplate.getForEntity(anyString(), eq(IcndbResponse.class)))
                    // 1st call
                    .thenThrow(new RuntimeException("something went wrong"))

                    // 2nd call
                    .thenReturn(buildResponse(remoteJoke));

            return restTemplate;
        }

        @Bean
        @Primary
        public JokeRepository jokeRepository() {
            final Joke dbJoke = new Joke(2L, 2L, DB_JOKE);

            final JokeRepository jokeRepository = mock(JokeRepository.class);
            when(jokeRepository.findAll()).thenReturn(Collections.singletonList(dbJoke));

            return jokeRepository;
        }
    }
}
