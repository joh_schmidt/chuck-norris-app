package chuck.norris;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.test.context.ActiveProfiles;

import static com.jayway.restassured.RestAssured.get;

@Category(DockerTest.class)
@ActiveProfiles("integrationTest")
public class ChuckNorrisApplicationDockerTest {
    @Test
    public void getHealthCheck() throws Exception {
        get("/health").then().assertThat().statusCode(200);
    }
}