package chuck.norris.domain;

import chuck.norris.persistence.Joke;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import java.util.Set;

/**
 * A Joke from {@code http://api.icndb.com/jokes/random}.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor
public class RemoteJoke {
    private Long id;
    private String joke;
    private Set<String> categories;

    public static RemoteJoke toRemoteJoke(final Joke joke) {
        RemoteJoke remoteJoke = new RemoteJoke();
        remoteJoke.setJoke(joke.getJoke());

        return remoteJoke;
    }
}
