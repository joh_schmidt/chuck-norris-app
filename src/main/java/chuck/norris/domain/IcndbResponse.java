package chuck.norris.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

/**
 * Response from {@code http://api.icndb.com/jokes/random}.
 * Wraps a {@code RemoteJoke}.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class IcndbResponse {
    private String type;
    private RemoteJoke value;
}
