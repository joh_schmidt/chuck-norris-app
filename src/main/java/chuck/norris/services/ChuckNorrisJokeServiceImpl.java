package chuck.norris.services;

import chuck.norris.domain.IcndbResponse;
import chuck.norris.domain.RemoteJoke;
import chuck.norris.persistence.Joke;
import chuck.norris.persistence.JokeRepository;
import com.google.common.collect.ImmutableList;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Random;

import static chuck.norris.domain.RemoteJoke.toRemoteJoke;
import static chuck.norris.persistence.Joke.fromRemoteJoke;

@Service
@Slf4j
public class ChuckNorrisJokeServiceImpl implements ChuckNorrisJokeService {
    private static final String SUCCESS = "success";

    private final RestTemplate restTemplate;
    private final JokeRepository jokeRepository;

    @Setter
    @Value("${remoteApi.url}")
    private String remoteApiUrl;

    @Autowired
    public ChuckNorrisJokeServiceImpl(final RestTemplate restTemplate, final JokeRepository jokeRepository) {
        this.restTemplate = restTemplate;
        this.jokeRepository = jokeRepository;
    }

    @Override
    @HystrixCommand(fallbackMethod = "getJokeFromDatabase", commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "5000")
    })
    public RemoteJoke getJokeFromRemote() {
        log.info("about to send a request to {}", remoteApiUrl);
        final ResponseEntity<IcndbResponse> response = restTemplate.getForEntity(remoteApiUrl, IcndbResponse.class);

        if (response.getStatusCode() != HttpStatus.OK || !response.getBody().getType().equals(SUCCESS)) {
            throw new RuntimeException("response was not ok");
        }

        final RemoteJoke remoteJoke = response.getBody().getValue();

        if (jokeRepository.findById(remoteJoke.getId()) == null) {
            jokeRepository.save(fromRemoteJoke(remoteJoke));
        }

        return remoteJoke;
    }

    private RemoteJoke getJokeFromDatabase() {
        log.warn("fallback triggered");
        final List<Joke> jokes = ImmutableList.copyOf(jokeRepository.findAll());

        if (jokes.size() == 0) {
            throw new RuntimeException("db is empty");
        }

        final Random randomizer = new Random();
        final Joke joke = jokes.get(randomizer.nextInt(jokes.size()));
        return toRemoteJoke(joke);
    }
}
