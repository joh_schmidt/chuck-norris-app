package chuck.norris.services;

import com.github.tomakehurst.wiremock.WireMockServer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.Options.DYNAMIC_PORT;

@Component
@Profile("icndbMock")
@Slf4j
public class IcndbMock implements InitializingBean, DisposableBean {
    public static final String MOCK_JOKE = "For Chuck Norris, NP-Hard = O(1).";
    private static final String MOCK_ANSWER = "{ \"type\": \"success\", \"value\": { \"id\": 481, \"joke\": \"" + MOCK_JOKE + "\", \"categories\": [\"nerdy\"] } }";

    private final WireMockServer wireMockServer = new WireMockServer(DYNAMIC_PORT);

    @Autowired
    private ChuckNorrisJokeService jokeService;


    @Override
    public void afterPropertiesSet() throws Exception {
        wireMockServer.start();

        final String remoteApiUrl = "http://localhost:" + wireMockServer.port() + "/joke";
        log.info("set remoteApiUrl to {}");
        jokeService.setRemoteApiUrl(remoteApiUrl);

        wireMockServer.stubFor(
                get(urlEqualTo("/joke"))
                        .willReturn(
                                aResponse()
                                        .withHeader("Content-Type", "application/json")
                                        .withBody(MOCK_ANSWER)
                                        .withStatus(200)
                        )
        );
    }

    @Override
    public void destroy() throws Exception {

    }
}
