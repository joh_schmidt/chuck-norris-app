package chuck.norris.services;

import chuck.norris.domain.RemoteJoke;

/**
 * Retrieves a joke from {@code http://api.icndb.com/jokes/random}.
 */
public interface ChuckNorrisJokeService {
    RemoteJoke getJokeFromRemote();

    void setRemoteApiUrl(String remoteApiUrl);
}
