package chuck.norris.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class ControllerExceptionHandler {
    @ExceptionHandler(Exception.class)
    public ModelAndView defaultExceptionHandler(final Exception e) throws Exception {
        return new ModelAndView("error", "exception", e);
    }
}
