package chuck.norris.controller;

import chuck.norris.domain.RemoteJoke;
import chuck.norris.services.ChuckNorrisJokeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomepageController {
    @Autowired
    private ChuckNorrisJokeService jokeService;

    @RequestMapping("/")
    public ModelAndView homepage() {
        final RemoteJoke joke = jokeService.getJokeFromRemote();
        return new ModelAndView("home", "joke", joke);
    }
}