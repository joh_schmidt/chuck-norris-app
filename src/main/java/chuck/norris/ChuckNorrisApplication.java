package chuck.norris;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@ComponentScan(basePackageClasses = ChuckNorrisApplication.class)
@EnableCircuitBreaker
@RestController
public class ChuckNorrisApplication {
    public static void main(final String[] args) {
        SpringApplication.run(ChuckNorrisApplication.class, args);
    }

    @RequestMapping("/health")
    public String home() {
        return "OK";
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
