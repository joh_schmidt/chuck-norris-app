package chuck.norris.persistence;

import chuck.norris.domain.RemoteJoke;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JokeRepository extends CrudRepository<Joke, Long> {
    RemoteJoke findById(Long id);
}
