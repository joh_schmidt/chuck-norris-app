package chuck.norris.persistence;

import chuck.norris.domain.RemoteJoke;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor
@Entity
public class Joke {
    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private Long remoteId;

    @Column(nullable = false)
    private String joke;

    public static Joke fromRemoteJoke(final RemoteJoke remoteJoke) {
        Joke joke = new Joke();
        joke.setRemoteId(remoteJoke.getId());
        joke.setJoke(remoteJoke.getJoke());

        return joke;
    }
}
