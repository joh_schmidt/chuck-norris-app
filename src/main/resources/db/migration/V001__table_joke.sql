CREATE TABLE `joke` (
  `id`        BIGINT(20)   NOT NULL AUTO_INCREMENT,
  `joke`      VARCHAR(255) NOT NULL,
  `remote_id` BIGINT(20)   NOT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;