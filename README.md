Chuck-Norris-App
================

![Codeship Status for joh_schmidt/chuck-norris-app](https://codeship.com/projects/a62561e0-2fc7-0134-a19e-0e4aa18abd32/status?branch=master)

Diese Webapp gibt zufällige, via http://api.icndb.com/jokes/random abgefragte Chuck-Norris-Witze aus. Die Rückgaben werden in einer Datenbank gespeichert und als Fallback verwendet, sollte die externe REST-API nicht erreichbar sein. 

# Ausführen der Tests

Das Projekt basiert auf Maven. Es gibt reine Unit-Tests auf Basis von Mockito, Integrations-Tests, die einen Spring-Kontext mit Teilen der Konfiguration starten, sowie Integrations-Tests, die gegen die komplette App und die Datenbank (jeweils in separaten Docker-Containern gestartet) testen.

Die Tests werden wie folgt gestartet:

```
git clone https://joh_schmidt@bitbucket.org/joh_schmidt/chuck-norris-app.git
cd chuck-norris-app
mvn verify
```

# Starten der Webanwendung

## ohne lokal installierte MySQL-Datenbank
Anwendung und Datenbank werden in zwei separaten Docker-Containern gestartet:
```
git clone https://joh_schmidt@bitbucket.org/joh_schmidt/chuck-norris-app.git
cd chuck-norris-app
mvn docker:start
```
Mit ```mvn docker:stop``` werden die Docker-Container wieder gestoppt.

## mit lokal installierter MySQL-Datenbank
Zuerst müssen Datenbank und User angelegt werden:
```
CREATE DATABASE chuck CHARACTER SET utf8 COLLATE utf8_bin;
GRANT ALL PRIVILEGES ON chuck.* TO 'chuck'@'localhost' IDENTIFIED BY 'norris';
FLUSH PRIVILEGES;
```

Anschließend kann die Applikation wie folgt gestartet werden:
```
git clone https://joh_schmidt@bitbucket.org/joh_schmidt/chuck-norris-app.git
cd chuck-norris-app
mvn spring-boot:run
```